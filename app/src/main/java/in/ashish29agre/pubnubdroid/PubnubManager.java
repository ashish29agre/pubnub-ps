package in.ashish29agre.pubnubdroid;

import com.pubnub.api.Pubnub;

/**
 * Created by Ashish on 09/01/16.
 */
public class PubnubManager {
    private static PubnubManager ourInstance = new PubnubManager();

    public static PubnubManager getInstance() {
        return ourInstance;
    }

    private Pubnub pubnub;

    private PubnubManager() {
        pubnub = new Pubnub(BuildConfig.PUBLISHER_KEY, BuildConfig.SUBSCRIBER_KEY, BuildConfig.SECRET_KEY);
    }

    public Pubnub getPubnub() {
        return pubnub;
    }

    public void destroy() {
        pubnub.shutdown();
    }
}
