package in.ashish29agre.pubnubdroid;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.pubnub.api.PubnubError;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {


    private static final Logger logger = Logger.getLogger(MainActivity.class.getSimpleName());
    private static final String CHAT = "CHAT";
    private static final String CONTENT = "CONTENT";

    private PubnubManager pubnubManager;
    private Publisher publisher;
    private Subscriber subscriber;
    private List<String> messages;
    private MessageAdapter adapter;
    private MessageHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initPubnub();
        initHandler();
    }

    private void initHandler() {
        handler = new MessageHandler();
    }


    private void initPubnub() {
        pubnubManager = PubnubManager.getInstance();
        publisher = new Publisher(pubnubManager, publisherCallback, CHAT);
        subscriber = new Subscriber(pubnubManager, subscriberCallback, CHAT);
    }

    private void initViews() {
        messages = new ArrayList<>();
        adapter = new MessageAdapter(messages);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.msg_recy_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        ImageView btnSend = (ImageView) findViewById(R.id.btn_send);
        final EditText editText = (EditText) findViewById(R.id.msg);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                publisher.publish(editText.getText().toString());
                editText.setText("");
            }
        });
    }

    Subscriber.SubscriberCallback subscriberCallback = new Subscriber.SubscriberCallback() {
        @Override
        public void onFailure(Object reason) {

        }

        @Override
        public void onDisconnect(Object reason) {

        }

        @Override
        public void onSuccess(Object reason) {
            logger.info("Success: " + reason.toString());
            Message message = handler.obtainMessage();
            message.obj = reason;
            handler.sendMessage(message);
        }

        @Override
        public void onReconnect(Object reason) {

        }

        @Override
        public void onConnect(Object reason) {

        }
    };

    Publisher.PublisherCallback publisherCallback = new Publisher.PublisherCallback() {

        @Override
        public void onSuccess(Object message) {
            logger.info("Success: " + message.toString());
        }

        @Override
        public void onFailure(PubnubError error) {
            logger.warning("Error: " + error.getErrorString());
        }
    };


    private class MessageHandler extends Handler {

        MessageHandler() {

        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.obj instanceof String) {
                String msgContent = (String) msg.obj;
                messages.add(msgContent);
                adapter.notifyDataSetChanged();
            }
        }
    }
}
