package in.ashish29agre.pubnubdroid;

import com.pubnub.api.Callback;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

/**
 * Created by Ashish on 09/01/16.
 */
public class Subscriber extends Callback {

    private PubnubManager pubnubManager;
    private SubscriberCallback callback;

    Subscriber(PubnubManager pubnubManager, SubscriberCallback callback, String... channels) {
        this.callback = callback;
        try {
            pubnubManager.getPubnub().subscribe(channels, this);
        } catch (PubnubException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void successCallback(String channel, Object message) {
        super.successCallback(channel, message);
        callback.onSuccess(message);
    }

    @Override
    public void successCallback(String channel, Object message, String timetoken) {
        super.successCallback(channel, message, timetoken);
    }

    @Override
    public void errorCallback(String channel, PubnubError error) {
        super.errorCallback(channel, error);
        callback.onFailure(error);
    }


    @Override
    public void connectCallback(String channel, Object message) {
        super.connectCallback(channel, message);
        callback.onConnect(message);
    }

    @Override
    public void reconnectCallback(String channel, Object message) {
        super.reconnectCallback(channel, message);
        callback.onReconnect(message);
    }

    @Override
    public void disconnectCallback(String channel, Object message) {
        super.disconnectCallback(channel, message);
        callback.onDisconnect(message);
    }

    interface SubscriberCallback {
        void onFailure(Object reason);

        void onDisconnect(Object reason);

        void onSuccess(Object reason);

        void onReconnect(Object reason);

        void onConnect(Object reason);
    }
}
